@echo off
call mvn clean package
call docker build -t com.mycompany/eva3joseEra .
call docker rm -f eva3joseEra
call docker run -d -p 9080:9080 -p 9443:9443 --name eva3joseEra com.mycompany/eva3joseEra